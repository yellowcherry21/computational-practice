from typing import List


def get_indices(arr: list, x) -> List[int]:
    matches = []# type: List[int]
    i = 0
    while i < len(arr):
        if arr[i] == x:
            matches.append(i)
        i = i+1
    return matches


assert get_indices(['a', 'a', 'b', 'a', 'b', 'a'], 'a') == [0, 1, 3, 5]
assert get_indices([1, 5, 5, 2, 7], 7) == [4]
assert get_indices([1, 5, 5, 2, 7], 5) == [1, 2]
assert get_indices([1, 5, 5, 2, 7], 8) == []
assert get_indices([8, 8, 8, 8, 8], 8) == [0, 1, 2, 3, 4]
assert get_indices([8, 8, 7, 8, 8], 8) == [0, 1, 3, 4]
assert get_indices([True, False, True, False], True) == [0, 2]
assert get_indices([True, False, True, False], False) == [1, 3]