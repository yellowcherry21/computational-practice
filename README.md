1. [The edit distance code.](https://bitbucket.org/yellowcherry21/computational-practice/src/master/edist.py)
2. [the number of digits of the number of possible permutations code](https://bitbucket.org/yellowcherry21/computational-practice/src/master/fact.py)
3. [Indices of all occurrences of an item in the list code.](https://bitbucket.org/yellowcherry21/computational-practice/src/master/indfun.py)
4. [Group-task code](https://bitbucket.org/yellowcherry21/computational-practice/src/master/start.py)