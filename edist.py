string1: str = input()
string2: str = input()


def edit_distance(s, t) -> int:
    if len(s) == 0:
        return len(t)
    elif len(t) == 0:
        return len(s)
    else:
        q = 0
        if s[len(s)-1] != t[len(t)-1]:
            q = 1
        d1: int = int(edit_distance(s.rstrip(s[len(s)-1]), t)) + 1
        d2: int = int(edit_distance(s, t.rstrip(t[len(t)-1]))) + 1
        d3: int = int(edit_distance(s.rstrip(s[len(s)-1]), t.rstrip(t[len(t)-1]))) + q
        return min(d1, d2, d3)


print('The edit distance is: ', edit_distance(string1, string2))


assert edit_distance('kitten', 'sitting') == 3
assert edit_distance('honda', 'hyundai') == 3
assert edit_distance('elephant', 'relevant') == 3
assert edit_distance('billiard', 'billjard') == 1
assert edit_distance('michael', 'mikael') == 2
